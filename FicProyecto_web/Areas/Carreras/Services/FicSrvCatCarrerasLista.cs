﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static FicProyecto_web.Models.FicCarrera;

namespace FicProyecto_web.Areas.Carreras.Services
{
    
    public class FicSrvCatCarrerasLista
    {
        HttpClient FicCliente = new HttpClient();
        public FicSrvCatCarrerasLista()
        {
            FicCliente.BaseAddress = new Uri("https://localhost:44344/");//cambira por mi puerto de api
            FicCliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<List<eva_cat_carreras>> FicGetListCatCarreras()
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/carreras/getcarreras/"); 
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_cat_carreras>>(FicRespuesta);
            }
            return new List<eva_cat_carreras>();
        }
        public async Task<List<cat_generales>> FicGetListCatGenerales()
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/carreras/getgenerales/");
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<cat_generales>>(FicRespuesta);
            }
            return new List<cat_generales>();
        }
        public async Task<List<eva_cat_carreras>> FicGetListCatCarreraID(Int16 idcarrera)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/carreras/getcarrerasid?IdCarrera=" + idcarrera);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_cat_carreras>>(FicRespuesta);
            }
            return new List<eva_cat_carreras>();
        }
        public async Task<bool> FicAddCarrera(eva_cat_carreras m)
        {
            string url = "api/carreras/add";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            if (response.IsSuccessStatusCode)
            {
                //response.Content.ReadAsStringAsync()
            }
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> FicUpdateCarrera(eva_cat_carreras m)
        {
            string url = "api/carreras/update";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            if (response.IsSuccessStatusCode)
            {
                //response.Content.ReadAsStringAsync()
            }
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> FicDeleteCatCarreraID(Int16 idcarrera)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/carreras/delete?IdCarrera=" + idcarrera);
            return FicResponse.IsSuccessStatusCode;
        }

        public async Task<List<cat_tipos_generales>> FicGetListCatGeneralesTipo()
        {
            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/generales/getgeneralestipo/");
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<cat_tipos_generales>>(FicRespuesta);
            }
            return new List<cat_tipos_generales>();
        }
    }
}