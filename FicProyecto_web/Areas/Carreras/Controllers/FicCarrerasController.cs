﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FicProyecto_web.Areas.Carreras.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static FicProyecto_web.Models.FicCarrera;

namespace FicProyecto_web.Areas.Carreras.Controllers
{
    [Area("Carreras")] //Se indica el area del controllador
    public class FicCarrerasController : Controller
    {

        // GET: FicCarreras
        public IActionResult Index()
        {
            try
            {
                FicSrvCatCarrerasLista FicServicio = new FicSrvCatCarrerasLista();
                cat_generales_consulta fic = new cat_generales_consulta();
                fic.eva_cat_carreras = FicServicio.FicGetListCatCarreras().Result;
                fic.cat_generales = FicServicio.FicGetListCatGenerales().Result;
                fic.cat_tipos_generales = FicServicio.FicGetListCatGeneralesTipo().Result;

                ViewBag.Title = "Catalogo de Carreras";
                return View(fic);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicAdd()
        {
            try
            {
                FicSrvCatCarrerasLista FicServicio = new FicSrvCatCarrerasLista();
                cat_carreras_agregar temp = new cat_carreras_agregar();
                temp.cat_generales = FicServicio.FicGetListCatGenerales().Result;
                ViewBag.Title = "Agregar Carreras ";
                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicDetail(Int16 idcarrera)
        {
            try
            {
                FicSrvCatCarrerasLista FicServicio = new FicSrvCatCarrerasLista();
                cat_carreras_agregar temp = new cat_carreras_agregar();
                temp.cat_generales = FicServicio.FicGetListCatGenerales().Result;
                temp.eva_cat_carreraslist = FicServicio.FicGetListCatCarreraID(idcarrera).Result;

                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicEdit(Int16 idcarrera)
        {
            try
            {
                FicSrvCatCarrerasLista FicServicio = new FicSrvCatCarrerasLista();
                cat_carreras_agregar temp = new cat_carreras_agregar();
                temp.cat_generales = FicServicio.FicGetListCatGenerales().Result;
                temp.eva_cat_carreraslist = FicServicio.FicGetListCatCarreraID(idcarrera).Result;
                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public async Task<IActionResult> FicUpdateCarrera(cat_carreras_agregar m)
        {
            m.eva_cat_carreras.FechaUltMod = DateTime.Now;
            if (m.eva_cat_carreras.Activo == "on")
            {
                m.eva_cat_carreras.Activo = "S";
            }
            else
            {
                m.eva_cat_carreras.Activo = "N";
            }
            if (m.eva_cat_carreras.Borrado == "on")
            {
                m.eva_cat_carreras.Borrado = "S";
            }
            else
            {
                m.eva_cat_carreras.Borrado = "N";
            }
            FicSrvCatCarrerasLista FicServicio = new FicSrvCatCarrerasLista();
            bool FicLista = await FicServicio.FicUpdateCarrera(m.eva_cat_carreras);

            return RedirectToAction("Index");
        }
        public async Task<IActionResult> FicAddCarrera(cat_carreras_agregar m)
        {

            m.eva_cat_carreras.FechaReg = DateTime.Now;
            m.eva_cat_carreras.FechaUltMod = DateTime.Now;//obtengo la fecha de mofificacion deszd el.sisgema
            if (m.eva_cat_carreras.Activo == "on")
            {
                m.eva_cat_carreras.Activo = "S";
            }
            else
            {
                m.eva_cat_carreras.Activo = "N";
            }
            if (m.eva_cat_carreras.Borrado == "on")
            {
                m.eva_cat_carreras.Borrado = "S";
            }
            else
            {
                m.eva_cat_carreras.Borrado = "N";
            }
            FicSrvCatCarrerasLista FicServicio = new FicSrvCatCarrerasLista();
            bool FicLista = await FicServicio.FicAddCarrera(m.eva_cat_carreras);

            TempData["Success"] = "Se agrego correctamente!";
            return RedirectToAction("Index");
        }
        public IActionResult FicDelete(Int16 idcarrera)
        {
            try
            {
                FicSrvCatCarrerasLista FicServicio = new FicSrvCatCarrerasLista();
                bool a = FicServicio.FicDeleteCatCarreraID(idcarrera).Result;
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                throw;
            }
        }

      
    }
}