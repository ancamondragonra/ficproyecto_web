﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FicProyecto_web.Models
{
    public class FicCarrera
    {
        public class eva_cat_carreras
        {
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            [Key]
            [Required]
            public Int16 IdCarrera { get; set; }
            [StringLength(20)]
            public string ClaveCarrera { get; set; }
            [StringLength(20)]
            public string ClaveOficial { get; set; }
            [StringLength(100)]
            public string DesCarrera { get; set; }
            [StringLength(10)]
            public string Alias { get; set; }
           // public Int16 IdAreaDepto { get; set; }
          //  public rh_cat_areas_deptos rh_cat_areas_deptos { get; set; }
            public Int16 IdTipoGenGradoEscolar { get; set; }
           // public cat_tipos_generales cat_tipos_generales { get; set; }
            public Int16 IdGenGradoEscolar { get; set; }
           // public cat_generales cat_generales { get; set; }
            public Nullable<DateTime> FechaReg { get; set; }
            public Nullable<DateTime> FechaUltMod { get; set; }
            [StringLength(20)]
            public string UsuarioReg { get; set; }
            [StringLength(20)]
            public string UsuarioMod { get; set; }
            [StringLength(1)]
            public string Activo { get; set; }
            [StringLength(1)]
            public string Borrado { get; set; }
            [StringLength(20)]
            public string NombreCorto { get; set; }
            public int Creditos { get; set; }
            public Int16 IdTipoGenModalidad { get; set; }
           // public cat_tipos_generales cat_tipos_generales_modalidad { get; set; }
            public Int16 IdGenModalidad { get; set; }
            // public cat_generales cat_generales_modalidad { get; set; }
            [DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy}", ApplyFormatInEditMode = true)]
            public Nullable<DateTime> FechaIni { get; set; }
            [DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy}", ApplyFormatInEditMode = true)]
            public Nullable<DateTime> FechaFin { get; set; }
        }//class eva_cat_carreras OK

        public class eva_cat_especialidades
        {
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public Int16 IdEspecialidad { get; set; }
            [StringLength(20)]
            public string DesEspecialidad { get; set; }
            [StringLength(1)]
            public string Activo { get; set; }
            [StringLength(1)]
            public string Borrado { get; set; }
            public Nullable<DateTime> FechaReg { get; set; }
            [StringLength(20)]
            public string UsuarioReg { get; set; }
            public Nullable<DateTime> FechaUltMod { get; set; }
            [StringLength(20)]
            public string UsuarioMod { get; set; }
        }

  
        public class eva_carreras_especialidades
        {
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            [Key]
            [Required]
            public Int16 IdCarreraEspecilidad { get; set; }
            public Int16 IdEspecialidad { get; set; }
            public eva_cat_especialidades eva_cat_especialidades { get; set; }
            public Int16 IdCarrera { get; set; }
            public eva_cat_carreras eva_cat_carreras { get; set; }
            [StringLength(1)]
            public string Activo { get; set; }
            [StringLength(1)]
            public string Borrado { get; set; }
            public Nullable<DateTime> FechaIni { get; set; }
            public Nullable<DateTime> FechaFin { get; set; }
            public Nullable<DateTime> FechaReg { get; set; }
            public Nullable<DateTime> FechaUltMod { get; set; }
            [StringLength(20)]
            public string UsuarioReg { get; set; }
            [StringLength(20)]
            public string UsuarioMod { get; set; }
        }


        public class cat_tipos_generales
        {
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            [Key]
            [Required]
            public Int16 IdTipoGeneral { get; set; }
            public string DesTipo { get; set; }
            public Nullable<DateTime> FechaReg { get; set; }
            public Nullable<DateTime> FechaUltMod { get; set; }
            [StringLength(20)]
            public string UsuarioReg { get; set; }
            [StringLength(20)]
            public string UsuarioMod { get; set; }
            [StringLength(1)]
            public string Activo { get; set; }
            [StringLength(1)]
            public string Borrado { get; set; }
        }
        public class cat_generales
        {
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            [Key]
            [Required]
            public Int16 IdGeneral { get; set; }
            public Int16 IdTipoGeneral { get; set; }

            public string Clave { get; set; }
            public string DesGeneral { get; set; }
            public string IdLlaveClasifica { get; set; }
            public string Referencia { get; set; }

            public DateTime? FechaReg { get; set; }
            public Nullable<DateTime> FechaUltMod { get; set; }
            [StringLength(20)]
            public string UsuarioReg { get; set; }
            [StringLength(20)]
            public string UsuarioMod { get; set; }
            [StringLength(1)]
            public string Activo { get; set; }
            [StringLength(1)]
            public string Borrado { get; set; }
        }

        /*AYUDANTE */
        public class cat_carreras_agregar
        {
            public eva_cat_carreras eva_cat_carreras { get; set; }
            public List<cat_generales> cat_generales { get; set; }
            public List<eva_cat_carreras> eva_cat_carreraslist { get; set; }
        }
        public class cat_exportar_importar
        {
            public List<cat_tipos_generales> cat_tipos_generales { get; set; }
            public List<cat_generales> cat_generales { get; set; }
            public List<eva_cat_carreras> eva_cat_carreras { get; set; }
            public List<eva_cat_especialidades> eva_cat_especialidades { get; set; }
            public List<eva_carreras_especialidades> eva_carreras_especialidades { get; set; }
        }
        public class cat_generales_agregar
        {
        
            public cat_generales cat_generales { get; set; }
            public List<cat_tipos_generales> cat_tipos_generales { get; set; }
        }
        public class cat_generales_editar
        {
            public List<cat_generales> cat_generales { get; set; }
            public List<cat_tipos_generales> cat_tipos_generales { get; set; }
        }
        public class cat_generales_consulta
        {
            public List<eva_cat_carreras> eva_cat_carreras { get; set; }
            public List<cat_generales> cat_generales { get; set; }
            public List<cat_tipos_generales> cat_tipos_generales { get; set; }
        }
        public class eva_carreras_especialdiades_agregar
        {
            public eva_carreras_especialidades eva_carreras_especialidades { get; set; }
            public List<eva_cat_carreras> eva_cat_carreras { get; set; }
            public List<eva_cat_especialidades> eva_cat_especialidades { get; set; }
        }
        public class cat_carreras_especialidades
        {
            public eva_cat_carreras eva_cat_carreras { get; set; }
            public List<eva_carreras_especialidades> eva_carreras_especialidades { get; set; }
            public List<eva_cat_especialidades> eva_cat_especialidades { get; set; }
        }
    }
}
