﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FicProyecto_web.Areas.Carreras.Services;
using Microsoft.AspNetCore.Mvc;
using static FicProyecto_web.Models.FicCarrera;

namespace FicProyecto_web.Areas.Generales.Controllers
{
    [Area("Generales")]
    public class FicGeneralesController : Controller
    {
        /*------------------- GENERALES  CRUD------------------------------ */
        public IActionResult Index()
        {
          
            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                List<cat_generales> FicLista = FicServicio.FicGetListCatGenerales().Result;
                ViewBag.Title = "Catalogo de Generales";
                return View(FicLista);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        

        public IActionResult FicGeneralAdd()
        {
            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                cat_generales_agregar temp = new cat_generales_agregar();
                temp.cat_tipos_generales = FicServicio.FicGetListCatGeneralesTipo().Result;
                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public IActionResult FicGeneralDetalle(Int16 idtipo)
        {
            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                cat_exportar_importar temp = new cat_exportar_importar();
                temp.cat_generales = FicServicio.FicGetListCatGeneralesID(idtipo).Result;
                temp.cat_tipos_generales = FicServicio.FicGetListCatGeneralesTipoID(temp.cat_generales[0].IdTipoGeneral).Result;
                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public async Task<IActionResult> FicAddGeneral(cat_generales_agregar m)
        {
            m.cat_generales.FechaReg = DateTime.Now;
            m.cat_generales.FechaUltMod = DateTime.Now;
            m.cat_generales.UsuarioReg = "FicIbarra";
            m.cat_generales.UsuarioMod = m.cat_generales.UsuarioReg;
            if (m.cat_generales.Activo == "on")
            {
                m.cat_generales.Activo = "S";
            }
            else
            {
                m.cat_generales.Activo = "N";
            }
            if (m.cat_generales.Borrado == "on")
            {
                m.cat_generales.Borrado = "S";
            }
            else
            {
                m.cat_generales.Borrado = "N";
            }
            FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
            bool FicLista = await FicServicio.FicAddGeneral(m.cat_generales);
            return RedirectToAction("Index");
        }
        public IActionResult FicDeleteGeneral(Int16 idgeneral)
        {
            try
            {
                 FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                bool a = FicServicio.FicDeleteCatGeneralesID(idgeneral).Result;
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public  IActionResult FicGeneralUpdate(Int16 idGeneral)
        {
            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                cat_generales_editar temp = new cat_generales_editar();
                temp.cat_generales = FicServicio.FicGetListCatGeneralesID(idGeneral).Result;
                temp.cat_tipos_generales = FicServicio.FicGetListCatGeneralesTipo().Result;

                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public async Task<IActionResult> FicUpdateGeneral(cat_generales_agregar m)
        {
            m.cat_generales.UsuarioReg = "FicIbarra";
            m.cat_generales.UsuarioMod = m.cat_generales.UsuarioReg;
            m.cat_generales.FechaUltMod = DateTime.Now;
            if (m.cat_generales.Activo == "on")
            {
                m.cat_generales.Activo = "S";
            }
            else
            {
                m.cat_generales.Activo = "N";
            }
            if (m.cat_generales.Borrado == "on")
            {
                m.cat_generales.Borrado = "S";
            }
            else
            {
                m.cat_generales.Borrado = "N";
            }
            FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
            bool FicLista = await FicServicio.FicUpdateGenerales(m.cat_generales);

            return RedirectToAction("Index");
        }

        /*------------------- TIPO CRUD------------------------------ */
        public IActionResult ListaTipo()
        {

            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                List<cat_tipos_generales> FicLista = FicServicio.FicGetListCatGeneralesTipo().Result;
                ViewBag.Title = "Catalogo de Generales Tipo";
                return View(FicLista);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicAddTipo()
        {
            try
            {
                ViewBag.Title = "Agregar Tipo";
                return View();
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public async Task<IActionResult> FicTipoAdd(cat_tipos_generales m)
        {
            m.FechaReg = DateTime.Now;
            m.FechaUltMod = DateTime.Now;
            m.UsuarioReg = "FicIbarra";
            m.UsuarioMod = m.UsuarioReg;
            if (m.Activo == "on")
            {
                m.Activo = "S";
            }
            else
            {
                m.Activo = "N";
            }
            if (m.Borrado == "on")
            {
                m.Borrado = "S";
            }
            else
            {
                m.Borrado = "N";
            }
            FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
            bool FicLista = await FicServicio.FicAddTipo(m);

            return RedirectToAction("ListaTipo");
        }
        public IActionResult FicEditTipo(Int16 idtipo)
        {
            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                var temp = FicServicio.FicGetListCatGeneralesTipoID(idtipo).Result;
                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public async Task<IActionResult> FicUpdateTipo(cat_tipos_generales m)
        {
            m.UsuarioReg = "FicIbarra";
            m.UsuarioMod = m.UsuarioReg;
            m.FechaUltMod = DateTime.Now;
            if (m.Activo == "on")
            {
                m.Activo = "S";
            }
            else
            {
                m.Activo = "N";
            }
            if (m.Borrado == "on")
            {
                m.Borrado = "S";
            }
            else
            {
                m.Borrado = "N";
            }
            FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
            bool FicLista = await FicServicio.FicUpdateTipo(m);

            return RedirectToAction("ListaTipo");
        }
        public IActionResult FicDetialTipo(Int16 idtipo)
        {
            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                var temp = FicServicio.FicGetListCatGeneralesTipoID(idtipo).Result;
                return View(temp);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicDeleteTipo(Int16 idtipo)
        {
            try
            {
                FicSrvCatGeneralesLista FicServicio = new FicSrvCatGeneralesLista();
                bool a = FicServicio.FicDeleteCatGeneralesTipoID(idtipo).Result;
                return RedirectToAction("ListaTipo");
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}