﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static FicProyecto_web.Models.FicCarrera;

namespace FicProyecto_web.Areas.Especialidades.Services
{
    public class FicSrvEspecialidades
    {
        HttpClient FicCliente = new HttpClient();
        public FicSrvEspecialidades()
        {
            FicCliente.BaseAddress = new Uri("https://localhost:44344/");//cambira por mi puerto de api
            FicCliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<List<eva_cat_especialidades>> FicGetEspecialidadesList()
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/getespecialidades/");
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_cat_especialidades>>(FicRespuesta);
            }
            return new List<eva_cat_especialidades>();
        }
        public async Task<bool> FicAddEspecialidad(eva_cat_especialidades m)
        {
            string url = "api/especialidades/addespecialidad";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> FicDeleteEspecialidad(Int16 id)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/especialidadesdelete?id=" + id);
            return FicResponse.IsSuccessStatusCode;
        }
        public async Task<List<eva_cat_especialidades>> FicGetListcat_especialidadesID(Int16 id)
        {
            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/especialidadesid?id=" + id);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_cat_especialidades>>(FicRespuesta);
            }
            return new List<eva_cat_especialidades>();
        }
        public async Task<bool> FicUpdateEspecialidad(eva_cat_especialidades m)
        {
            string url = "api/especialidades/especialidadesidupdate";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }

        /*---------------------------------------------------------------------------*/
        public async Task<List<eva_carreras_especialidades>> FicGetEspecialidadesCarreraList()
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/getespecarreras/");
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_carreras_especialidades>>(FicRespuesta);
            }
            return new List<eva_carreras_especialidades>();
        }

        public async Task<bool> FicAddEspecialidadCarrera(eva_carreras_especialidades m)
        {
            string url = "api/especialidades/addespecialidadcarrera";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }
        public async Task<List<eva_carreras_especialidades>> FicGetList_especialidades_carrerasID(Int16 id)
        {
            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/getespecarrerasid/?id=" + id);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_carreras_especialidades>>(FicRespuesta);
            }
            return new List<eva_carreras_especialidades>();
        }
        public async Task<bool> FicDeleteEspecialidadEC(Int16 id)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/especialidadesecdelete?id=" + id);
            return FicResponse.IsSuccessStatusCode;
        }
        public async Task<bool> FicUpdateEspecialidadEC(eva_carreras_especialidades m)
        {
            string url = "api/especialidades/especialidadesiECdupdate";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }
        public async Task<List<eva_cat_especialidades>> FicGetEspecialidadesListCarrera(Int16 id )
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/GetEspecialidadesCarreraEC?id=" + id);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_cat_especialidades>>(FicRespuesta);
            }
            return new List<eva_cat_especialidades>();
        }
        public async Task<List<eva_carreras_especialidades>> FicGetEspecialidadesListCarrera2(Int16 id)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/GetEspecialidadesCarreraEC2?id=" + id);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_carreras_especialidades>>(FicRespuesta);
            }
            return new List<eva_carreras_especialidades>();
        }
        public async Task<List<eva_carreras_especialidades>> FicGetEspecialidadesListCarrera3(Int16 id)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/especialidades/especialidadescarrerasid?id=" + id);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_carreras_especialidades>>(FicRespuesta);
            }
            return new List<eva_carreras_especialidades>();
        }
    }
}