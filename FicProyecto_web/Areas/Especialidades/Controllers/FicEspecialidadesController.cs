﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FicProyecto_web.Areas.Carreras.Services;
using FicProyecto_web.Areas.Especialidades.Services;
using Microsoft.AspNetCore.Mvc;
using static FicProyecto_web.Models.FicCarrera;

namespace FicProyecto_web.Areas.Especialidades.Controllers
{
    [Area("Especialidades")]
    public class FicEspecialidadesController : Controller
    {
        public IActionResult Index()
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            var data= FicServicio.FicGetEspecialidadesList().Result;
            return View(data);
        }
        public IActionResult FicAdd()
        {
            return View();
        }
        public async Task<IActionResult> FicAddEspecialidad(eva_cat_especialidades m)
        {

            m.FechaReg = DateTime.Now;
            m.FechaUltMod = DateTime.Now;
            if (m.Activo == "on")
            {
                m.Activo = "S";
            }
            else
            {
                m.Activo = "N";
            }
            if (m.Borrado == "on")
            {
                m.Borrado = "S";
            }
            else
            {
                m.Borrado = "N";
            }
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            bool FicLista = await FicServicio.FicAddEspecialidad(m);

            TempData["Success"] = "Se agrego correctamente!";
            return RedirectToAction("Index");
        }
        public IActionResult FicDeleteEspecialidadl(Int16 id)
        {
            try
            {
                FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
                bool a = FicServicio.FicDeleteEspecialidad(id).Result;
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicDetalle(Int16 id)
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            var FicLista =  FicServicio.FicGetListcat_especialidadesID(id).Result;
            return View(FicLista);
        }
        public IActionResult FicEditar(Int16 id)
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            var FicLista = FicServicio.FicGetListcat_especialidadesID(id).Result;
            return View(FicLista);
        }
        public async Task<IActionResult> FicEditEspecialidad(eva_cat_especialidades m)
        {
            m.UsuarioMod = "FicIbarra";
            m.UsuarioReg = m.UsuarioMod;
            m.FechaUltMod = DateTime.Now;
            if (m.Activo == "on")
            {
                m.Activo = "S";
            }
            else
            {
                m.Activo = "N";
            }
            if (m.Borrado == "on")
            {
                m.Borrado = "S";
            }
            else
            {
                m.Borrado = "N";
            }
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            bool FicLista = await FicServicio.FicUpdateEspecialidad(m);

            TempData["Success"] = "Se actualizo correctamente!";
            return RedirectToAction("Index");
        }

        /*---------------------------------------------------------------------------*/
        public IActionResult IndexEC()
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            var data = FicServicio.FicGetEspecialidadesCarreraList().Result;
            return View(data);
        }

        public IActionResult FicAddEC()
        {

            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            FicSrvCatCarrerasLista ficSrvCarreras = new FicSrvCatCarrerasLista();
            eva_carreras_especialdiades_agregar data = new eva_carreras_especialdiades_agregar();
            data.eva_cat_especialidades = FicServicio.FicGetEspecialidadesList().Result;
            data.eva_cat_carreras = ficSrvCarreras.FicGetListCatCarreras().Result;
            return View(data);
        }
        public async Task<IActionResult> FicAddEspecialidadCarrera(eva_carreras_especialdiades_agregar m)
        {
            m.eva_carreras_especialidades.FechaReg = DateTime.Now;
            m.eva_carreras_especialidades.FechaUltMod = DateTime.Now;
            if (m.eva_carreras_especialidades.Activo == "on")
            {
                m.eva_carreras_especialidades.Activo = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Activo = "N";
            }
            if (m.eva_carreras_especialidades.Borrado == "on")
            {
                m.eva_carreras_especialidades.Borrado = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Borrado = "N";
            }
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            bool FicLista = await FicServicio.FicAddEspecialidadCarrera(m.eva_carreras_especialidades);

            TempData["Success"] = "Se agrego correctamente!";
            return RedirectToAction("IndexEC");
        }
        public IActionResult FicDetalleEC(Int16 id)
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            var FicLista = FicServicio.FicGetList_especialidades_carrerasID(id).Result;
            return View(FicLista);
        }
        public IActionResult FicUpdateEC(Int16 id)
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            var FicLista = FicServicio.FicGetList_especialidades_carrerasID(id).Result;
        
            FicSrvCatCarrerasLista ficSrvCarreras = new FicSrvCatCarrerasLista();
            eva_carreras_especialdiades_agregar data = new eva_carreras_especialdiades_agregar();
            data.eva_cat_especialidades = FicServicio.FicGetEspecialidadesList().Result;
            data.eva_cat_carreras = ficSrvCarreras.FicGetListCatCarreras().Result;
            data.eva_carreras_especialidades = FicLista[0];
            return View(data);
        }
        public async Task<IActionResult> FicEditEC(eva_carreras_especialdiades_agregar m)
        {
            m.eva_carreras_especialidades.UsuarioMod = "FicIbarra";
            m.eva_carreras_especialidades.UsuarioReg = m.eva_carreras_especialidades.UsuarioMod;
            m.eva_carreras_especialidades.FechaUltMod = DateTime.Now;
            if (m.eva_carreras_especialidades.Activo == "on")
            {
                m.eva_carreras_especialidades.Activo = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Activo = "N";
            }
            if (m.eva_carreras_especialidades.Borrado == "on")
            {
                m.eva_carreras_especialidades.Borrado = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Borrado = "N";
            }
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
              bool FicLista = await FicServicio.FicUpdateEspecialidadEC(m.eva_carreras_especialidades);

            TempData["Success"] = "Se actualizo correctamente!";
            return RedirectToAction("IndexEC");
        }
        public IActionResult FicDeleteEspecialidadEC(Int16 id)
        {
            try
            {
                FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
                bool a = FicServicio.FicDeleteEspecialidadEC(id).Result;
                return RedirectToAction("IndexEC");
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicCarreraEspecialdiades(Int16 idcarrera)
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            FicSrvCatCarrerasLista ficSrvCarreras = new FicSrvCatCarrerasLista();
            cat_carreras_especialidades data = new cat_carreras_especialidades();
            data.eva_cat_carreras=ficSrvCarreras.FicGetListCatCarreraID(idcarrera).Result.First();
            data.eva_cat_especialidades = FicServicio.FicGetEspecialidadesListCarrera(idcarrera).Result;
            data.eva_carreras_especialidades = FicServicio.FicGetEspecialidadesListCarrera2(idcarrera).Result;
            return View(data);
        }

        public IActionResult FicAddECIndividual(Int16 idcarrera)
        {

            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            FicSrvCatCarrerasLista ficSrvCarreras = new FicSrvCatCarrerasLista();
            eva_carreras_especialdiades_agregar data = new eva_carreras_especialdiades_agregar();

            data.eva_cat_carreras = ficSrvCarreras.FicGetListCatCarreraID(idcarrera).Result;
            data.eva_cat_especialidades = FicServicio.FicGetEspecialidadesList().Result;

            return View(data);
        }
        public async Task<IActionResult> FicAddEspecialidadCarreraIndividual(eva_carreras_especialdiades_agregar m)
        {
            m.eva_carreras_especialidades.UsuarioMod = "FicIbarra";
            m.eva_carreras_especialidades.UsuarioMod = m.eva_carreras_especialidades.UsuarioMod;
            m.eva_carreras_especialidades.FechaReg = DateTime.Now;
            m.eva_carreras_especialidades.FechaUltMod = DateTime.Now;
            if (m.eva_carreras_especialidades.Activo == "on")
            {
                m.eva_carreras_especialidades.Activo = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Activo = "N";
            }
            if (m.eva_carreras_especialidades.Borrado == "on")
            {
                m.eva_carreras_especialidades.Borrado = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Borrado = "N";
            }
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            bool FicLista = await FicServicio.FicAddEspecialidadCarrera(m.eva_carreras_especialidades);

            TempData["Success"] = "Se agrego correctamente!";
            return RedirectToAction("FicCarreraEspecialdiades", new { idcarrera = m.eva_carreras_especialidades.IdCarrera });
        }
        public IActionResult FicDeleteEspecialidadECIndividual(Int16 id,Int16 idvolver)
        {
            try
            {
                FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
                bool a = FicServicio.FicDeleteEspecialidadEC(id).Result;
                return RedirectToAction("FicCarreraEspecialdiades", new { idcarrera = idvolver });
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public IActionResult FicDetalleECIndividual(Int16 id)
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            FicSrvCatCarrerasLista ficSrvCarreras = new FicSrvCatCarrerasLista();
            eva_carreras_especialdiades_agregar data = new eva_carreras_especialdiades_agregar();
            data.eva_cat_especialidades = FicServicio.FicGetEspecialidadesList().Result;
            data.eva_carreras_especialidades= FicServicio.FicGetEspecialidadesListCarrera3(id).Result.First();
            data.eva_cat_carreras = ficSrvCarreras.FicGetListCatCarreraID(data.eva_carreras_especialidades.IdCarrera).Result;
            return View(data);
        }
        public IActionResult FicEditarECIndividual(Int16 id)
        {
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            var FicLista = FicServicio.FicGetList_especialidades_carrerasID(id).Result.First();

            FicSrvCatCarrerasLista ficSrvCarreras = new FicSrvCatCarrerasLista();
            eva_carreras_especialdiades_agregar data = new eva_carreras_especialdiades_agregar();
            data.eva_cat_especialidades = FicServicio.FicGetEspecialidadesList().Result;
            data.eva_cat_carreras = ficSrvCarreras.FicGetListCatCarreras().Result;
            data.eva_carreras_especialidades = FicLista;
            return View(data);
        }
        public async Task<IActionResult> FicEditECIndividual(eva_carreras_especialdiades_agregar m)
        {
            m.eva_carreras_especialidades.UsuarioMod = "FicIbarra";
            m.eva_carreras_especialidades.UsuarioReg = m.eva_carreras_especialidades.UsuarioMod;
            m.eva_carreras_especialidades.FechaUltMod = DateTime.Now;
            if (m.eva_carreras_especialidades.Activo == "on")
            {
                m.eva_carreras_especialidades.Activo = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Activo = "N";
            }
            if (m.eva_carreras_especialidades.Borrado == "on")
            {
                m.eva_carreras_especialidades.Borrado = "S";
            }
            else
            {
                m.eva_carreras_especialidades.Borrado = "N";
            }
            FicSrvEspecialidades FicServicio = new FicSrvEspecialidades();
            bool FicLista = await FicServicio.FicUpdateEspecialidadEC(m.eva_carreras_especialidades);

            TempData["Success"] = "Se actualizo correctamente!";
            return RedirectToAction("FicCarreraEspecialdiades", new { idcarrera = m.eva_carreras_especialidades.IdCarrera });
        }
    }
}