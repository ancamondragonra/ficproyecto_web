﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static FicProyecto_web.Models.FicCarrera;

namespace FicProyecto_web.Areas.Carreras.Services
{
    
    public class FicSrvCatGeneralesLista
    {
        HttpClient FicCliente = new HttpClient();
        public FicSrvCatGeneralesLista()
        {
            FicCliente.BaseAddress = new Uri("https://localhost:44344/");//cambira por mi puerto de api
            FicCliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<List<cat_generales>> FicGetListCatGenerales()
        {
            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/generales/getgenerales/"); 
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<cat_generales>>(FicRespuesta);
            }
            return new List<cat_generales>();
        }
        public async Task<List<cat_generales>> FicGetListCatGeneralesID(Int16 id)
        {
            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/generales/getcarrerasid/?IdGeneral=" + id);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<cat_generales>>(FicRespuesta);
            }
            return new List<cat_generales>();
        }
        public async Task<bool> FicAddGeneral(cat_generales m)
        {
            string url = "api/generales/addgeneral";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> FicDeleteCatGeneralesID(Int16 IdGeneral)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/generales/delete?IdGeneral=" + IdGeneral);
            return FicResponse.IsSuccessStatusCode;
        }
        public async Task<bool> FicUpdateGenerales(cat_generales m)
        {
            string url = "api/generales/update";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }

        /*--------- CAT GENERALES TIPO----------*/
        public async Task<List<cat_tipos_generales>> FicGetListCatGeneralesTipo()
        {
            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/generales/getgeneralestipo/");
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<cat_tipos_generales>>(FicRespuesta);
            }
            return new List<cat_tipos_generales>();
        }
        public async Task<bool> FicAddTipo(cat_tipos_generales m)
        {
            string url = "api/generales/addtipo";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> FicUpdateTipo(cat_tipos_generales m)
        {
            string url = "api/generales/updatetipo";
            HttpResponseMessage response = await FicCliente.PostAsync(
                new Uri(string.Format(FicCliente.BaseAddress + url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(m), Encoding.UTF8, "application/json")
            );
            return response.IsSuccessStatusCode;
        }

        public async Task<List<cat_tipos_generales>> FicGetListCatGeneralesTipoID(Int16 IdTipoGeneral)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/generales/getgeneralidtipo?IdTipoGeneral=" + IdTipoGeneral);
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<cat_tipos_generales>>(FicRespuesta);
            }
            return new List<cat_tipos_generales>();
        }
        public async Task<bool> FicDeleteCatGeneralesTipoID(Int16 IdTipoGeneral)
        {

            HttpResponseMessage FicResponse = await FicCliente.GetAsync("api/generales/deletetipo/?IdTipoGeneral=" + IdTipoGeneral);
            return FicResponse.IsSuccessStatusCode;
        }
    }
}